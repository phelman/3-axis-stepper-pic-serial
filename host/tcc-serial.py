#!/usr/bin/python
# -*- coding: utf-8 -*-
################################################################################
################################################################################
#
# Instituto Federal de Educacao, Ciencia e Tecnologia de Goias
# Departamento de area IV
# Engenharia de Controle e Automacao
# Goiania, Maio de 2016
# Aluno: Pedro Henrique Maione Campos
# Orientador: Dr. Ildeu Lucio Siqueira
#
################################################################################
# Referencia:
# https://pythonhosted.org/pyserial/shortintro.html
# https://docs.python.org/2/library/configparser.html
################################################################################
# Descricao:
#
# Envia uma string, um dado de cada vez pela porta serial e recebe o mesmo dado
# para confirmacao
#
################################################################################
# Importa os modulos com funcoes necessarias
import serial       # Comunicacao Serial
import re           # RegExp
import ConfigParser # Para ler o arquivo de configuracao
import sys
import os
import string
from glob import glob

################################################################################
# Parameters:
# port - Device name or None.
# baudrate (int) - Baud rate such as 9600 or 115200 etc.
# bytesize - Number of data bits.
# Possible values: FIVEBITS, SIXBITS, SEVENBITS, EIGHTBITS
# parity - Enable parity checking.
# Possible values: PARITY_NONE, PARITY_EVEN, PARITY_ODD PARITY_MARK,
# PARITY_SPACE
# stopbits - Number of stop bits.
# Possible values: STOPBITS_ONE, STOPBITS_ONE_POINT_FIVE, STOPBITS_TWO
# timeout (float) - Set a read timeout value.
# xonxoff (bool) - Enable software flow control.
# rtscts (bool) - Enable hardware (RTS/CTS) flow control.
# dsrdtr (bool) - Enable hardware (DSR/DTR) flow control.
# write_timeout (float) - Set a write timeout value.
# inter_byte_timeout (float) - Inter-character timeout, None to disable
# (default).
#
# Raises:
# ValueError - Will be raised when parameter are out of range, e.g. baud
# rate, data bits.
# SerialException - In case the device can not be found or can not be
# configured.
# The parameter baudrate can be one of the standard values: 50, 75, 110, 134,
# 150, 200, 300, 600, 1200, 1800, 2400, 4800, 9600, 19200, 38400, 57600,
# 115200. These are well supported on all platforms.
# Standard values above 115200, such as: 230400, 460800, 500000, 576000, 921600,
# 1000000, 1152000, 1500000, 2000000, 2500000, 3000000, 3500000, 4000000 also
# work on many platforms and devices.

################################################################################
# CONSTANTES
################################################################################
TITLE_CHAR = '='
TITLE_WIDTH = 50
SERIAL_ENDLINE_CHAR = '0d'.decode('hex')
SERIAL_TARGET_IDLE = '1f'.decode('hex')

################################################################################
# VARIAVEIS GLOBAIS
################################################################################
config_filename = 'serial-config.cfg' #
serial_port_config_section = 'SerialCOM' # Secao do arquivo de config
serial_port_param = { #
    'port' : '/dev/ttyUSB0', # Diretorio da porta serial
    'baudrate' : 19200, # Taxa de transmissao (em inteiro)
    'bytesize' : 'EIGHTBITS', # Numero de bits de dados
    'parity' : 'PARITY_NONE', # Paridade
    'stopbits' : 'STOPBITS_ONE', # Numero de bits de parada
    'timeout' : 2, # Tempo maximo para tentativa de conexao
    'xonxoff' : False, #
    'rtscts' : False, #
    'dsrdtr' : False, #
    'write_timeout' : 1, #
    'inter_byte_timeout' : '', #
}#
comserial = serial.Serial()#
cnc_filename = '' # Nome do arquivo de texto do programa CNC
################################################################################
# DEFINICAO DE FUNCOES
################################################################################

#------------------------------------ FUNCAO -----------------------------------
# Imprime no console o titulo centralizado
#-------------------------------------------------------------------------------
def wait_ok():
    raw_input('\n\nPressione ENTER para continuar.')
    return

#------------------------------------ FUNCAO -----------------------------------
# Imprime no console o titulo centralizado
#-------------------------------------------------------------------------------
def print_title (title, char, width):
    os.system('clear')
    print '\n', char * width
    print title.center(width, ' ')
    print char * width , '\n'
    return

#------------------------------------ FUNCAO -----------------------------------
# Abre a porta serial
#-------------------------------------------------------------------------------
def open_port ():

    if comserial.isOpen():
        print 'A porta serial [%s] ja esta ativada!' % (comserial.port)
    else:
        try:
            comserial.port = serial_port_param['port']
            comserial.baudrate = serial_port_param['baudrate']
            comserial.open()
            # Abre a porta serial
            print 'A porta serial [%s] foi ativada!' % (comserial.port)
        except serial.SerialException:
            print 'Erro: Falha ao ativar a porta [%s] !' % (comserial.port)
    wait_ok()
    return
#------------------------------------ FUNCAO -----------------------------------
# Fecha a porta serial, caso estiver aberta
#-------------------------------------------------------------------------------
#
def close_port ():
    if comserial.isOpen():
        comserial.close()
        print '\nA porta serial [%s] foi desativada!' % (comserial.port)
    else:
        print '\nErro: A porta [%s] nao esta ativa!' % (comserial.port)
    wait_ok()
    return
#------------------------------------ FUNCAO -----------------------------------
# Testa a conexao da porta serial
#-------------------------------------------------------------------------------
def test_com ():
    if comserial.isOpen():
        try:
            print 'A porta [%s] esta ativa.' % (serial_port_param['port'])
            print_serial_port()
            comserial.flushOutput()
            str_teste = string.printable
            print 'Enviando: %s' % str_teste
            comserial.write(str_teste)
            comserial.flushInput()
            # Le a quantidade de caracteres enviado para teste
            str_read = comserial.read(len(str_teste))
            print 'Recebido: %s' % str_read
            if str_teste == str_read:
                print 'COMUNICACAO : OK !!!'
            else:
                print 'COMUNICACAO : ERRO !!!'
        except serial.SerialTimeoutException:
            print '\nErro: Tempo de envio esgotado!'
        except serial.SerialException:
            print '\nErro: Nao foi possivel comunicar com a porta serial'
        else:
            print '\nErro: A porta nao esta ativada!'
            
    wait_ok()
    return
#------------------------------------ FUNCAO -----------------------------------
# Altera um parametro no arquivo de configuracao do programa
#-------------------------------------------------------------------------------
#
def set_config (dict_param, section, key, value):
    # TODO: Verifica se o valor e' valido para o parametro
    # Escreve no arquivo de configuracao
    dict_param[key] = value
    write_config(config_filename, section, dict_param)
    print '\nParametro alterado com sucesso!'
    print '[%s] : %s\n' % (key, dict_param[key])
    return
#------------------------------------ FUNCAO -----------------------------------
# Imprime no console os parametros do arquivo de configuracao
#-------------------------------------------------------------------------------
def end_program():
    if comserial.isOpen():
        print 'Fechanda a porta [%s]' % comserial.port
        comserial.close()
        wait_ok()
    sys.exit()
    return # esta linha nao e' executada
#------------------------------------ FUNCAO -----------------------------------
# Imprime no console os parametros do arquivo de configuracao
#-------------------------------------------------------------------------------
def print_parameters ():
    old_param = read_config(config_filename, serial_port_config_section)
    print '-' * TITLE_WIDTH
    print 'Arquivo : %s' % (config_filename)
    print '%20s %s' % ('[PARAMETRO]', 'VALOR')
    print '-' * TITLE_WIDTH
    for key in old_param:
        print '%20s %s' % ('['+key+']', old_param[key])
    print '-' * TITLE_WIDTH
    return

#------------------------------------ FUNCAO -----------------------------------
# Imprime no console os parametros do arquivo de configuracao
#-------------------------------------------------------------------------------
def print_serial_port ():
    print '-' * TITLE_WIDTH
    print '%25s %s\n' % ('[PARAMETRO]', 'VALOR')
    comserial_param = comserial.getSettingsDict()
    for key in comserial_param:
        print '%25s %s' % ('['+key+']', comserial_param[key])
    print '-' * TITLE_WIDTH
    return
#------------------------------------ FUNCAO -----------------------------------
# Escreve o arquivo de configuracao
#-------------------------------------------------------------------------------
# config_filename : Nome do arquivo de configuracao
# section: Nome da secao de configuracao do arquivo
# dict_param: Dicionario com parametros de configuracao
#-------------------------------------------------------------------------------
def write_config (config_filename, section, dict_param):
    try:
        config = ConfigParser.SafeConfigParser(allow_no_value=True)
        config.add_section(section)
        for key in dict_param:
            config.set(section, key, str(dict_param[key]))
            with open(config_filename, 'wb') as configfile:
                config.write(configfile)
    except ConfigParser.Error:
        print 'Erro: Nao foi possivel escrever no arquivo de configuracao'
    return
#------------------------------------ FUNCAO -----------------------------------
# Le o arquivo de configuracao e retorna os valores dos parametros
#-------------------------------------------------------------------------------
def read_config (config_filename, section):
    try:
        config = ConfigParser.SafeConfigParser(allow_no_value=True)
        config.read(config_filename)
        param = config.items(section)
    except ConfigParser.Error:
        print 'Erro: Nao foi possivel escrever no arquivo de configuracao'
        param = {}
    return dict(param)
#------------------------------------ FUNCAO -----------------------------------
# Lista os arquivos
#-------------------------------------------------------------------------------
def choose_file():
    '''Gera uma lista com os arquivos de texto.'''
    filename = ''
    print '\nDigite o diretorio para procurar o arquivo de programa CNC.'
    print 'Deixe em branco para procurar no diretorio atual.'
    print 'Digite [sair] para retornar ao menu anterior.'
    
    search_dir = raw_input('\n > ')
    if search_dir == '':
        search_dir = './'
    elif search_dir == 'sair':
        return
    #
    dirlist = glob(search_dir + '*.nc')
    dirdict = {}
    if len(dirlist) == 0:
        print 'Nao existem arquivos de texto neste diretorio.'
    else:
        dirdict = {}
        index = 1
        for files in dirlist:
            dirdict[index] = files
            index += 1
        print '\nLista de arquivos de texto no diretorio: %s ' % search_dir
        for key in dirdict:
            print '[ ', key, ' ] ', dirdict[key]
        choice = int(raw_input('\nDigite o indice do arquivo. > '))
        if choice in dirdict:
            filename = dirdict[choice]
            print '\nEscolhido o arquivo : %s' % filename
        else:
            print '\nErro: Nao existe este indice de arquivo!'
    wait_ok()
    return filename

#------------------------------------ FUNCAO -----------------------------------
# Analisa sintaxe do arquivo de texto do programa CNC
#-------------------------------------------------------------------------------
def send_cnc_file(com1, filename):
    if filename == '':
        print '\nErro: Nao foi selecionado nenhum arquivo!'
    else:
        try:
            inf = open(filename, 'r')
            print 'Enviando arquivo [ %s ].' % filename
            print '-' * TITLE_WIDTH
            for line in inf:
                str_line = parse_str(line)
                if str_line != '':
                    serial_send_string(com1, str_line)
        finally:
            inf.close()
            print '\nArquivo enviado com sucesso!'
    return
#------------------------------------ FUNCAO -----------------------------------
# Trata uma string para ser enviada.
#-------------------------------------------------------------------------------
# Ignora linhas com comentarios e vazias, retornando uma string vazia
# Se nao:
# 1) Remove os espacos em branco (economizar espaco no buffer do microcontrolador)
# 2) Converte em caixa alta
# 3) Remove numeracao da linha se necessario
# 4)
#-------------------------------------------------------------------------------
def parse_str (str1):
    # Converte em caixa alta
    str1 = str1.upper()
    # Remove comentarios
    str1 = re.sub('\\(.*\\)', '', str1)
    # Verifica possiveis erros
    if re.match(r"^\s*$", str1):
        str1 = ''
    elif re.match(r'^/.*$', str1):
        str1 = ''
    else:
        # Substitui o caractere de fim de linha '\n' por '#'
        str1 = re.sub(r"\n", '#', str1)
        # Remove numeracao da linha se necessario
        str1 = re.sub(r"N[0-9]+\.?[0-9]?", '', str1)
        # Substitui G0 -> G00, G1 -> G01
        str1 = re.sub(r"G([0-9]\s+)", r"G0\1 ", str1)
        # Adiciona o sinal se nao existir
        str1 = re.sub(r"([XYZ])([0-9]+)", r"\1+\2", str1)
        # Padroniza a quant. de numeros apos X, Y e Z e sinal de deslocamento
        # str1 = re.sub("\\([XYZ]{1}[-+]?\\)\\([0-9]+[^0-9]+\\)\\([0-9]+\\)",
        # replace_fixed_width,
        # str1)
        # Padroniza
        #str1 = re.sub(r"([F])([0-9]+)\.?([0-9]+)", fix_number_size, str1)
        # Remove os espacos em branco
        str1 = re.sub (r"[ \t]*", '', str1)
        # Insere ';' caso nao tenha
        str1 = re.sub(r"[^;]$", ';', str1)
    # Retorna a string resultante
    return str1

#------------------------------------ FUNCAO -----------------------------------
# Envia uma string pela porta serial
#-------------------------------------------------------------------------------
def serial_send_string(ser, txtline):
    try:
        print '\nEnviando: %s' % txtline
        fb_string = ''
        for char in txtline:
            ser.write(char)
            while ser.inWaiting() < 1:
                pass # espera o buffer ter 1 char
            fb_char = ser.read(1)
            fb_string += unicode(fb_char)
            
            print '\nVerificacao: %s' % fb_string
    except:
        print '\nErro: Falha ao escrever na porta serial!'
        wait_ok()
    return

################################################################################
# MAIN
################################################################################
if __name__=='__main__':
    global cnc_filename
    # limpa a tela
    os.system('clear')
    # Imprime o cabecalho
    print '\n\n'
    print '==================================================='
    print 'Intituto Federal de Goias'
    print 'Engenharia de Controle e Automacao'
    print 'Trabalho de Conclusao de Curso'
    print 'Aluno: Pedro Maione'
    print 'Orientador: Dr. Ildeu Lucio Siqueira'
    print '==================================================='
    wait_ok()
    #===========================================================================
    while True:
        serial_port_param = read_config(config_filename, serial_port_config_section)
        print_title('MENU PRINCIPAL', TITLE_CHAR, TITLE_WIDTH)
        menu_principal = {'1' : 'ABRIR PORTA SERIAL',
                          '2' : 'FECHAR PORTA SERIAL',
                          '3' : 'TESTE DE COMUNICACAO',
                          '4' : 'ALTERAR PARAMETROS DE CONFIGURACAO',
                          '5' : 'ESCOLHER ARQUIVO DO PROGRAMA CNC',
                          '6' : 'TRANSMITIR ARQUIVO DO PROGRAMA CNC',
                          '9' : 'FINALIZAR O PROGRAMA'}
        options = menu_principal.keys()
        options.sort()
        for entry in options:
            print ' [ ', entry, ' ] ', menu_principal[entry]
        choice = raw_input('\n >> ')
        #---------------------------------------------------------------------------
        if choice == '1':
            print_title(menu_principal['1'], TITLE_CHAR, TITLE_WIDTH)
            print_parameters()
            open_port()
            #---------------------------------------------------------------------------
        elif choice == '2':
            print_title(menu_principal['2'], TITLE_CHAR, TITLE_WIDTH)
            close_port()
        #---------------------------------------------------------------------------
        elif choice == '3':
            print_title(menu_principal['3'], TITLE_CHAR, TITLE_WIDTH)
            test_com()
        #---------------------------------------------------------------------------
        elif choice == '4':
            # Alterar configuracao
            print_title(menu_principal['4'],TITLE_CHAR, TITLE_WIDTH)
            print_parameters()
            print '\nDigite \'sair\' para retornar ao menu principal. '
            key = raw_input('Digite o parametro a ser alterado: \n >> ')
            if key in serial_port_param:
                value = raw_input('\nDigite o novo valor para o parametro:\n [' + \
                                  key+'] : ')
                set_config(serial_port_param, serial_port_config_section, key, value)
            elif key == 'sair':
                pass
            else:
                print '\nErro: Parametro inexistente!\n'
        #---------------------------------------------------------------------------
        elif choice == '5':
            print_title(menu_principal['5'], TITLE_CHAR, TITLE_WIDTH)
            cnc_filename = choose_file()
        #---------------------------------------------------------------------------
        elif choice == '6':
            print_title(menu_principal['6'], TITLE_CHAR, TITLE_WIDTH)
            send_cnc_file(comserial, cnc_filename)
        #---------------------------------------------------------------------------
        elif choice == '9':
            print_title(menu_principal['9'], TITLE_CHAR, TITLE_WIDTH)
            end_program()
        #---------------------------------------------------------------------------
        else:
            print '\n Escolha invalida, escolha novamente'
            wait_ok()
        #---------------------------------------------------------------------------
    end_program()
# FIM DO ARQUIVO
