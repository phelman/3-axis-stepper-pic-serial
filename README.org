#+TITLE: Sistema embarcado para controle de motores de passo
#+AUTHOR: Pedro maione
#+EMAIL: pedromaione@protonmail.com
#+DESCRIPTION: Descrição do sistema embarcado

#+OPTIONS:   H:4 num:nil toc:2 p:t

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="/home/maione/documentos/org-notes/css/readtheorg/htmlize.css"/>
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="/home/maione/documentos/org-notes/css/readtheorg/readtheorg.css"/>

* Introdução

Este é o repositório de códigos do desenvolvimento do sistema embarcado de baixo custo
para acionar simultaneamente motores de passo, com comunicação por porta USB,
através de um FT232RL, e que interpreta intruções baseados no padrão EIA
RS-274D.

É utilizado um microcontrolador MICROCHIP PIC16F877A.

* Organização

O projeto está organizado conforme os diretórios abaixo:
 - scr :: Contém os códigos fonte para o firmware.
 - lkr :: Contém o /script/ para o MPLINK.
 - host :: Diretório com o /script/ em Python para transmissão de arquivos NC.
 - eagle :: Esquemáticos dos circuitos e /layout/ das PCIs.


