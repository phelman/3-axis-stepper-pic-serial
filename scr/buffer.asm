
;;;
;;; filename : buffer.asm
;;; Autor    : Pedro Maione
;;; Goiania, Dezembro de 2015
;;;


  title           "Buffer Functions"


;;;                                           INCLUDE HEADERS

  include         "def.inc"


;;;                                   FUNCOES /  VARIAVEIS [EXTERNAS]

  EXTERN          SendUSARTData, DelayMS


;;; FUNCOES /  VARIAVEIS [GLOBAIS]


  GLOBAL          FLAG_BUFFER_RX, FLAG_BUFFER_BLOCK
  GLOBAL          PTR_BUFFER_RX, PTR_BUFFER_BLOCK, BUFFER_BLOCK
  GLOBAL          WriteToBUFFER_RX, ReadFromBUFFER_RX, ResetBUFFER_RX
  GLOBAL          WriteToBUFFER_BLOCK, ReadFromBUFFER_BLOCK, ResetBUFFER_BLOCK
  GLOBAL          SendUSART_BUFFER_RX, SendUSART_BUFFER_BLOCK, CopyBUFFER_RX_To_BUFFER_BLOCK



;;;                                              CONSTANTES


_BUFF_SIZE                      SET     .40            ; Tamanho padrao da string
_BUFF_OFFSET                    SET     (_BUFF_SIZE-1) ; Deslocamento do primeiro para o ultimo byte


;;;                                              VARIAVEIS


__VAR_BUFFER_MACROS             UDATA_SHR
BUFF_TEMP                       res     1

__VAR_BUFFER_RX                 UDATA
BUFFER_RX                       res     _BUFF_SIZE
PTR_BUFFER_RX                   res     1
FLAG_BUFFER_RX                  res     1

__VAR_BUFFER_BLOCK              UDATA
BUFFER_BLOCK                    res     _BUFF_SIZE
PTR_BUFFER_BLOCK                res     1
FLAG_BUFFER_BLOCK               res     1



;;;                                                 CODE

__SRC_BUFFER                    CODE


;;;
;;;                                                MACROS
;;;



;;; ================================================================================================
;;;                                                MACRO
;;; Reseta os flag do BUFFER
;;; ------------------------------------------------------------------------------------------------
;;;                                              ARGUMENTOS
;;; FLAG : Flag para ser resetado
;;; ================================================================================================
_m_ResetBufferFlag  MACRO   FLAG
        BANKSEL     FLAG
        clrf        FLAG
        bsf         FLAG,EMPTY
        ENDM

;;; ================================================================================================
;;;                                                MACRO
;;; Escreve um byte no BUFFER atraves de acesso indireto ao registrador
;;; ------------------------------------------------------------------------------------------------
;;;                                              ARGUMENTOS
;;; BUFFER : Sequencia de 'n' registradores formando uma string
;;; PTR    : Ponteiro para o endereço a ser escrito o dado em WREG
;;; ================================================================================================
_m_WriteToBuffer    MACRO   BUFFER, PTR
        BANKISEL    BUFFER      ; Seleciona o buffer para enderecamento indireto
        BANKSEL     PTR
        movwf       BUFF_TEMP   ; Armazena o dado de WREG em BUFF_TEMP (multibank)
        movf        PTR,W
        movwf       FSR         ; FSR Aponta para o endereco desejado (PTR)
        movf        BUFF_TEMP,W
        movwf       INDF        ; Escreve no endereco indiretamente
        clrw                    ; Limpa o registrador WREG
        ENDM
;;; ================================================================================================
;;;                                                MACRO
;;; Le um byte no BUFFER atraves de acesso indireto ao registrador
;;; Limpa o BUFF_TEMP
;;; ------------------------------------------------------------------------------------------------
;;;                                              ARGUMENTOS
;;; BUFFER : Sequencia de 'n' registradores formando uma string
;;; PTR    : Ponteiro para o endereço do dado a ser lido e escrito em WREG
;;; ================================================================================================
_m_ReadFromBuffer   MACRO   BUFFER, PTR
        clrf        BUFF_TEMP   ; Limpa o BUFF_TEMP (multibank)
        BANKISEL    BUFFER
        BANKSEL     PTR
        movfw       PTR
        movwf       FSR         ; FSR Aponta para o endereco desejado (PTR)
        movfw       INDF        ; Le o dado
        ENDM
;;; ================================================================================================
;;;                                                MACRO
;;; Testa se o ponteiro aponta para o ultimo endereco do buffer
;;; Se sim seta o FLAG, bit FULL (definido no arquivo def.inc)
;;; ------------------------------------------------------------------------------------------------
;;;                                              ARGUMENTOS
;;; BUFFER : Sequencia de 'n' registradores formando uma string
;;; PTR    : Ponteiro para o endereço a ser testado
;;; FLAG   : Flag do Buffer, a ser setado
;;; ================================================================================================
_m_TestLastAddress  MACRO   BUFFER, PTR, FLAG
        BANKISEL    BUFFER
        BANKSEL     PTR
        movlw       LOW (BUFFER+_BUFF_OFFSET) ;
        xorwf       PTR,W                     ;
        skpnz                                 ; PTR é o ultimo byte de BUFFER?
        bsf         FLAG,FULL                 ; Sim, Seta o flag do buffer
        clrw                                  ; Limpa o WREG
        ENDM


;;; ================================================================================================
;;;                                                MACRO
;;; Reseta o BUFFER, escrevendo 0x00 em todos os enderecos
;;; PTR aponta para o primeiro endereco
;;; ------------------------------------------------------------------------------------------------
;;;                                              ARGUMENTOS
;;; BUFFER : Sequencia de 'n' registradores formando uma string
;;; PTR    : Ponteiro para o endereço do dado a ser lido e escrito em WREG
;;; FLAG   : Flag do Buffer
;;; ================================================================================================
_m_ResetBuffer      MACRO   BUFFER, PTR, FLAG
        LOCAL       _loop_ResetBuffer, _end_ResetBuffer
        BANKISEL    BUFFER      ; Seleciona o BUFFER para enderecamento indireto
        movlw       LOW BUFFER
        movwf       FSR         ; FSR Aponta para o primeiro endereco do buffer
_loop_ResetBuffer:
        clrf        INDF                      ; Limpa o Registrador indiretamente
        movlw       LOW (BUFFER+_BUFF_OFFSET) ;
        xorwf       FSR,W                     ;
        skpnz                                 ; FSR é o ultimo endereco do BUFFER?
        goto        _end_ResetBuffer          ; Sim
        incf        FSR,F                     ; Nao, aponta para o proximo e volta para o loop
        goto        _loop_ResetBuffer         ;
_end_ResetBuffer:
        BANKSEL     PTR
        movlw       LOW BUFFER
        movwf       PTR
        _m_ResetBufferFlag FLAG
        ENDM


;;;
;;;                                               ROTINAS
;;;


;;; ================================================================================================
;;;                                                ROTINA
;;; Escreve o byte de WREG no endereco apontado por PTR_BUFFER_RX no BUFFER_RX
;;; E testa se aponta para o ultimo endereco:
;;; Se sim: retorna
;;; Se nao: aponta para o proximo endereco e entao retorna
;;; ================================================================================================
WriteToBUFFER_RX:
        _m_WriteToBuffer    BUFFER_RX, PTR_BUFFER_RX
        _m_TestLastAddress  BUFFER_RX, PTR_BUFFER_RX, FLAG_BUFFER_RX
        btfss       FLAG_BUFFER_RX,FULL ; Testa se esta escrevendo no ultimo endereco
        incf        PTR_BUFFER_RX,F     ; Nao, aponta para o proximo endereco
        return                          ; Sim, apenas continua

;;; ================================================================================================
;;;                                                ROTINA
;;; Le o byte no endereco apontado por PTR_BUFFER_RX no BUFFER_RX e escreve em WREG
;;; E testa se aponta para o ultimo endereco:
;;; Se sim: retorna
;;; Se nao: aponta para o proximo endereco e entao retorna o dado em WREG
;;; ================================================================================================
ReadFromBUFFER_RX:
        _m_ReadFromBuffer   BUFFER_RX, PTR_BUFFER_RX
        movwf       BUFF_TEMP
        _m_TestLastAddress  BUFFER_RX, PTR_BUFFER_RX, FLAG_BUFFER_RX
        BANKSEL     FLAG_BUFFER_RX
        btfss       FLAG_BUFFER_RX,FULL ; Testa se esta escrevendo no ultimo endereco
        incf        PTR_BUFFER_RX,F     ; Nao, aponta para o proximo endereco
        movfw       BUFF_TEMP           ; Sim, apenas continua (o flag ja esta setado)
        return
;;; ================================================================================================
;;;                                                ROTINA
;;; Reseta o BUFFER_RX
;;; Escreve H'00' em todos os enderecos do BUFFER_RX
;;; ================================================================================================
ResetBUFFER_RX:
        _m_ResetBuffer      BUFFER_RX, PTR_BUFFER_RX, FLAG_BUFFER_RX
        return

;;; ================================================================================================
;;;                                     ROTINA : SendUSART_BUFFER_RX
;;; Envia o BUFFER_RX pela porta serial
;;; ================================================================================================
SendUSART_BUFFER_RX:
        BANKSEL     PTR_BUFFER_RX
        movlw       LOW BUFFER_RX
        movwf       PTR_BUFFER_RX ; Reseta o ponteiro
SendUSART_BUFFER_RX_loop:
        call        ReadFromBUFFER_RX ;
        call        SendUSARTData     ;
        movfw       BUFF_TEMP
        xorlw       _CHAR_RX_EOL
        skpz                                 ; PTR -> EOL ?
        goto        SendUSART_BUFFER_RX_loop ; Nao
        ;; EOL identificado
        call        ResetBUFFER_RX
        return



;;; ================================================================================================
;;;                                                ROTINA
;;; Escreve o byte de WREG no endereco apontado por PTR_BUFFER_BLOCK no BUFFER_BLOCK
;;; E testa se aponta para o ultimo endereco:
;;; Se sim: retorna
;;; Se nao: aponta para o proximo endereco e entao retorna
;;; ================================================================================================
WriteToBUFFER_BLOCK:
        _m_WriteToBuffer    BUFFER_BLOCK, PTR_BUFFER_BLOCK
        _m_TestLastAddress  BUFFER_BLOCK, PTR_BUFFER_BLOCK, FLAG_BUFFER_BLOCK
        BANKSEL     FLAG_BUFFER_BLOCK
        btfss       FLAG_BUFFER_BLOCK,FULL ; Testa se esta escrevendo no ultimo endereco
        incf        PTR_BUFFER_BLOCK,F     ; Nao, aponta para o proximo endereco
        return                             ; Sim, apenas continua

;;; ================================================================================================
;;;                                                ROTINA
;;; Le o byte no endereco apontado por PTR_BUFFER_BLOCK no BUFFER_BLOCK e escreve em WREG
;;; E testa se aponta para o ultimo endereco:
;;; Se sim: retorna
;;; Se nao: aponta para o proximo endereco e entao retorna o dado em WREG
;;; ================================================================================================
ReadFromBUFFER_BLOCK:
        _m_ReadFromBuffer   BUFFER_BLOCK, PTR_BUFFER_BLOCK
        movwf       BUFF_TEMP
        _m_TestLastAddress  BUFFER_BLOCK, PTR_BUFFER_BLOCK, FLAG_BUFFER_BLOCK
        BANKSEL     FLAG_BUFFER_BLOCK
        btfss       FLAG_BUFFER_BLOCK,FULL ; Testa se esta escrevendo no ultimo endereco
        incf        PTR_BUFFER_BLOCK,F     ; Nao, aponta para o proximo endereco
        movfw       BUFF_TEMP              ; Sim, apenas continua (o flag ja esta setado)
        return
;;; ================================================================================================
;;;                                                ROTINA
;;; Reseta o BUFFER_BLOCK
;;; Escreve H'00' em todos os enderecos do BUFFER_BLOCK
;;; ================================================================================================
ResetBUFFER_BLOCK:
        _m_ResetBuffer      BUFFER_BLOCK, PTR_BUFFER_BLOCK, FLAG_BUFFER_BLOCK
        return

;;; ================================================================================================
;;;                                     ROTINA : SendUSART_BUFFER_BLOCK
;;; Envia o BUFFER_BLOCK pela porta serial
;;; ================================================================================================
SendUSART_BUFFER_BLOCK:
        BANKSEL     PTR_BUFFER_BLOCK
        movlw       LOW BUFFER_BLOCK
        movwf       PTR_BUFFER_BLOCK ; Reseta o ponteiro
        _m_ResetBufferFlag FLAG_BUFFER_BLOCK
SendUSART_BUFFER_BLOCK_loop:
        call        ReadFromBUFFER_BLOCK ;
        movwf       BUFF_TEMP
        call        SendUSARTData        ;
        movlw       _CHAR_RX_EOL
        xorwf       BUFF_TEMP,W
        skpz                                    ; PTR -> EOL ?
        goto        SendUSART_BUFFER_BLOCK_loop ; Nao
        ;; EOL identificado
        call        ResetBUFFER_BLOCK
        return




;;; ================================================================================================
;;;                                ROTINA: CopyBUFFER_RX_To_BUFFER_BLOCK
;;;
;;; Le o byte no endereco apontado por PTR_BUFFER_RX no BUFFER_RX e escreve no endereco
;;;   apontado por PTR_BUFFER_BLOCK no BUFFER_BLOCK
;;; ================================================================================================
CopyBUFFER_RX_To_BUFFER_BLOCK:
        call        ResetBUFFER_BLOCK
        BANKISEL    BUFFER_BLOCK
        BANKSEL     PTR_BUFFER_BLOCK
        movlw       LOW BUFFER_BLOCK
        movwf       PTR_BUFFER_BLOCK
        BANKISEL    BUFFER_RX
        BANKSEL     PTR_BUFFER_RX
        movlw       LOW BUFFER_RX
        movwf       PTR_BUFFER_RX

CopyBUFFER_loop:
        call        ReadFromBUFFER_RX
        movwf       BUFF_TEMP
        call        WriteToBUFFER_BLOCK
        movlw       _CHAR_RX_EOL
        xorwf       BUFF_TEMP,W
        skpz
        goto        CopyBUFFER_loop

        call        ResetBUFFER_RX
        return







        END

;;;                                            FIM DO ARQUIVO

