
;;;
;;; filename : ascii.asm
;;; Autor    : Pedro Maione
;;; Goiania, Dezembro de 2015
;;;


  title           "ASCII Conversion Functions"


;;;                                           INCLUDE HEADERS

  include         "def.inc"
  include         "multi-byte-macros.inc"


;;;                                   FUNCOES /  VARIAVEIS [EXTERNAS]


        

;;; FUNCOES /  VARIAVEIS [GLOBAIS]

  GLOBAL          CONV_INPUT, CONV_RESULT
  GLOBAL          AsciiConvertDec2Bin, ResetCONV_INPUT


;;;                                              CONSTANTES





;;;                                              VARIAVEIS


__VAR_ASCII_CONVERT             UDATA
CONV_INPUT                      res     5 ; Caracteres a converter ex.: D'0103'
CONV_RESULT                     res     2 ; Resultado da conversao (Max D'9999' = H'270F')



;;;                                                 CODE

__SRC_ASCII_CONVERTER             CODE


;;;
;;;                                                MACROS
;;;



;;; ================================================================================================
;;;                                                MACRO
;;;
;;; ------------------------------------------------------------------------------------------------
;;;                                              ARGUMENTOS
;;;
;;;
;;; ================================================================================================




;;;
;;;                                               ROTINAS
;;;



;;; ================================================================================================
;;;                                         ROTINA : ResetCONV_INPUT
;;; 
;;;
;;; ================================================================================================
ResetCONV_INPUT:
        _m_Clr40    CONV_INPUT

        return
        
;;; ================================================================================================
;;;                                         ROTINA : ConvertAscii2Dec
;;;
;;; Adaptado de http://www.piclist.com/techref/microchip/math/radix/bu2b-5d16b-ph.htm
;;; Published in EPE Dec '03 issue. By PETER HEMSLEY
;;;
;;; ------------------------------------------------------------------------------------------------
;;; Representacao:
;;; A representacao abaixo é obtida convertendo potencias de 10 em potencias de 2.
;;;
;;; Seja 'X' um decimal entre 0 e 9:
;;;
;;; X * 1     = (X * 1)
;;; X * 10    = (X * 8)    + (X * 2)
;;; X * 100   = (X * 64)   + (X * 32)   + (X * 4)
;;; X * 1000  = (X * 512)  + (X * 256)  + (X * 128) + (X * 64)  + (X * 32) + (X * 8)
;;; X * 10000 = (X * 8192) + (X * 1024) + (X * 512) + (X * 256) + (X * 16)
;;;
;;; Conducive numbers to work with are:
;;; 2   (SHIFT LEFT to multiply by 2)
;;; 16  (NIBBLE SWAP to multiply by 16)
;;; 256 (The result goes into the high byte of the binary)
;;;
;;; Se usarmos SUBTRACT:
;;;
;;; X * 1     = (X * 1)
;;; X * 10    = (X * 8)    + (X * 2)
;;; X * 100   = (X * 64)   + (X * 32)   + (X * 4)
;;; X * 1000  = (X * 1024) - (X * 32)   + (X * 8)
;;; X * 10000 = (X * 8192) + (X * 2048) - (X * 256) + (X * 16)
;;;
;;; These five expressions can now be written in terms of conducive numbers and combined to give:
;;;
;;; N = (((D1 + D3 + D4 * 256) * 2 + D2 * 16 + D2 + D3 * 256) * 2 - D3 * 16 +
;;;     D2 * 16 + D1 + D4 * 16 * 256) * 2 + D4 * 16 + D0 - D4 * 256
;;; Where D0 = ones, D1 = tens, D2 = hundreds, D3 = thousands, D4 = ten thousands
;;;
;;;
;;; Convert 4 bytes Ascii representando numeros decimais em 2 bytes binarios
;;; A'0' == H'30'
;;;
;;; CONV_INPUT : 4 bytes,   LOW CONV_INPUT equiv. a unidades
;;;                         HIGH CONV_INPUT equiv. a dezenas
;;;
;;; CONV_RESULT: Resultado da conversao
;;;
;;; ================================================================================================
AsciiConvertDec2Bin:

        BANKSEL     CONV_RESULT
        clrf        CONV_RESULT
        clrf        CONV_RESULT+1

        ;; (D1 + D3) * 2
        movf        CONV_INPUT+1,W ; W = D1
        addwf       CONV_INPUT+3,W ; W = (D1 + D3)
        movwf       CONV_RESULT    ; NUM_LOW = W
        rlf         CONV_RESULT,F  ; NUM_LOW = NUM_LOW * 2

        ;; + D2 * 16 + D2
        swapf       CONV_INPUT+2,W ; W = D2 * 16
        addwf       CONV_INPUT+2,W ; W = (D2 * 16) + D2
        addwf       CONV_RESULT,F  ; NUM_LOW = NUM_LOW + ((D2 * 16) + D2)

        ;;  + (D4 * 2 + D3) * 256
        rlf         CONV_INPUT+4,W ; W = D4 * 2
        addwf       CONV_INPUT+3,W ; W = (D4 * 2) + D3
        movwf       CONV_RESULT+1  ; NUM_HI = ((D4 * 2) + D3) * 256

        rlf         CONV_RESULT,F   ; NUM_LOW = NUM_LOW * 2
        rlf         CONV_RESULT+1,F ; NUM_HI = NUM_HI * 2

        ;; - D3 * 16
        swapf       CONV_INPUT+3,W  ; W = D3 * 16
        subwf       CONV_RESULT,F   ; NUM_LOW = NUM_LOW - (D3 * 16)
        skpc                        ; NUM_LOW < 0 (underflow)
        decf        CONV_RESULT+1,F ;

        ;; + D2 * 16 + D1
        swapf       CONV_INPUT+2,W  ; W = D2 * 16
        addwf       CONV_INPUT+1,W  ; W = (D2 * 16) + D1
        addwf       CONV_RESULT,F   ; NUM_LOW = NUM_LOW + ((D2 * 16) + D1)
        skpnc                       ; NUM_LOW > 0xFF (overflow)
        incf        CONV_RESULT+1,F ;

        ;; + D4 * 16 + D0
        swapf       CONV_INPUT+4,W ; 
        addwf       CONV_INPUT,W   ;

        rlf         CONV_RESULT,F   ; * 2
        rlf         CONV_RESULT+1,F ;

        addwf       CONV_RESULT,F   ;
        skpnc                       ;
        incf        CONV_RESULT+1,F ;

        movf        CONV_INPUT+4,W  ; - D4 * 256
        subwf       CONV_RESULT+1,F ;

        swapf       CONV_INPUT+4,W  ; + D4 * 16 * 256 * 2
        addwf       CONV_RESULT+1,F ;
        addwf       CONV_RESULT+1,F ;

        return



        END

;;;                                            FIM DO ARQUIVO

