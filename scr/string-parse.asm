;;; Autor    : Pedro Maione
;;; Goiania, Dezembro de 2015
    title           "String Parser Functions"
    include         "def.inc"
    include         "multi-byte-macros.inc"
;;;
;;;                                   FUNCOES /  VARIAVEIS [EXTERNAS]
;;;

;;; main.asm
    EXTERN          MAIN_FLAG
    EXTERN          FUNCTION_G, FUNCTION_M, FUNCTION_F
    EXTERN          FUNCTION_X, FUNCTION_Y, FUNCTION_Z

;;; buffer.asm
    EXTERN          PTR_BUFFER_BLOCK, BUFFER_BLOCK
    EXTERN          ReadFromBUFFER_BLOCK, ResetBUFFER_BLOCK

;;; ascii.asm
    EXTERN          CONV_INPUT, CONV_RESULT
    EXTERN          AsciiConvertDec2Bin, ResetCONV_INPUT

;;; stepper.asm
    EXTERN          FLAG_MOTOR_X, FLAG_MOTOR_Y, FLAG_MOTOR_Z, STEP_DELAY
    EXTERN          MOTOR_X_COUNTER, MOTOR_Y_COUNTER, MOTOR_Z_COUNTER

;;; usart.asm
    EXTERN          SendUSARTData, INDEX


;;; FUNCOES /  VARIAVEIS [GLOBAIS]

    GLOBAL          ParseBUFFER_BLOCK, ComputeFUNCTION_F
    GLOBAL          ComputeFUNCTION_X, ComputeFUNCTION_Y, ComputeFUNCTION_Z

__VAR_PARSER_AUX                UDATA_SHR
CHAR_TEMP                       res     1

;;;                                                 CODE
__SRC_STRING_PARSER             CODE


_m_BranchOnMatch    MACRO   CHAR, _CHAR_FUN, _BRANCH
        movlw       _CHAR_FUN   ;
        xorwf       CHAR,W      ; CHAR == _CHAR_FUN ?
        bz          _BRANCH     ; Sim, vai para o _BRANCH
        ;; Nao, continua a execucao
        ENDM

_m_SkipIfNOMatch    MACRO   CHAR, _CHAR_FUN
        movlw       _CHAR_FUN   ;
        xorwf       CHAR,W      ; CHAR == _CHAR_FUN ?
        skpnz                   ; Nao, Pula proxima instrucao
        ;; Sim
        ENDM



;;; Le uma subtring de 'n' elementos do BUFFER_BLOCK e armazena em DST
_m_ReadString       MACRO   Count, DST
    VARIABLE        i
i = Count-1                     ; Incializa contador
    WHILE   i >= 0
        call        ReadFromBUFFER_BLOCK
        BANKSEL     DST+i
        movwf       DST+i
        movlw       0x30
        subwf       DST+i,F
i -= 1                          ; Decrementa contador
    ENDW

        ENDM


;;; ================================================================================================
ParseBUFFER_BLOCK:
        BANKSEL     PTR_BUFFER_BLOCK
        movlw       LOW BUFFER_BLOCK
        movwf       PTR_BUFFER_BLOCK ; PTR_BUFFER_BLOCK -> BUFFER_BLOCK[0]

ParseTestFun:
        call        ReadFromBUFFER_BLOCK
        movwf       CHAR_TEMP   ; Salva o dado para teste, chamando o macro
        _m_BranchOnMatch    CHAR_TEMP, _CHAR_PREP_FUN, ParseFunG ; CHAR_TEMP == _CHAR_PREP_FUN ?
        _m_BranchOnMatch    CHAR_TEMP, _CHAR_AUX_FUN, ParseFunM  ;
        _m_BranchOnMatch    CHAR_TEMP, _CHAR_FEED_FUN, ParseFunF ;
        _m_BranchOnMatch    CHAR_TEMP, _CHAR_X_FUN, ParseFunX
        _m_BranchOnMatch    CHAR_TEMP, _CHAR_Y_FUN, ParseFunY
        _m_BranchOnMatch    CHAR_TEMP, _CHAR_Z_FUN, ParseFunZ


        _m_BranchOnMatch    CHAR_TEMP, _CHAR_BLOCK_EOL, ParseBUFFER_BLOCK_End
        ;; Chegou ao fim da string e nao encontrou ';'

ParseBUFFER_BLOCK_Error:
        _m_SetErrorLED
        movlw       'E'
        call        SendUSARTData
        movlw       '1'
        call        SendUSARTData
        return

;;; ------------------------------------------------------------------------------------------------
ParseFunG:
        call        ResetCONV_INPUT
        _m_ReadString       2, CONV_INPUT

        call        AsciiConvertDec2Bin

        _m_Mov16    CONV_RESULT, FUNCTION_G

        _m_BranchOnMatch    FUNCTION_G, 0x00, FunG00
        _m_BranchOnMatch    FUNCTION_G, 0x01, FunG01

FunG00:
        bcf         MAIN_FLAG,G01
        goto        ParseTestFun

FunG01:
        ;; TODO: calcular o Feedrate
        bsf         MAIN_FLAG,G01
        goto        ParseTestFun


;;; ------------------------------------------------------------------------------------------------
;;;
;;;
ParseFunX:
        call        ReadFromBUFFER_BLOCK
        movwf       CHAR_TEMP
        BANKSEL     FLAG_MOTOR_X
        _m_SkipIfNOMatch    CHAR_TEMP, _CHAR_MINUS_SIGN
        bsf         FLAG_MOTOR_X, DIRECTION
        _m_SkipIfNOMatch    CHAR_TEMP, _CHAR_PLUS_SIGN
        bcf         FLAG_MOTOR_X, DIRECTION
        ;;
        call        ResetCONV_INPUT
        _m_ReadString       4, CONV_INPUT
        call        AsciiConvertDec2Bin
        _m_Mov16    CONV_RESULT, FUNCTION_X
        BANKSEL     FLAG_MOTOR_X
        bsf         FLAG_MOTOR_X,ENABLE
        goto        ParseTestFun


;;; ------------------------------------------------------------------------------------------------
;;;
;;;
ParseFunY:
        call        ReadFromBUFFER_BLOCK
        movwf       CHAR_TEMP
        BANKSEL     FLAG_MOTOR_Y
        _m_SkipIfNOMatch    CHAR_TEMP, _CHAR_MINUS_SIGN
        bsf         FLAG_MOTOR_Y, DIRECTION
        _m_SkipIfNOMatch    CHAR_TEMP, _CHAR_PLUS_SIGN
        bcf         FLAG_MOTOR_Y, DIRECTION
        ;;
        call        ResetCONV_INPUT
        _m_ReadString       4, CONV_INPUT
        call        AsciiConvertDec2Bin
        _m_Mov16    CONV_RESULT, FUNCTION_Y
        BANKSEL     FLAG_MOTOR_Y
        bsf         FLAG_MOTOR_Y,ENABLE

        goto        ParseTestFun
;;; ------------------------------------------------------------------------------------------------
;;;
;;;
ParseFunZ:
        call        ReadFromBUFFER_BLOCK
        movwf       CHAR_TEMP
        BANKSEL     FLAG_MOTOR_Z
        _m_SkipIfNOMatch    CHAR_TEMP, _CHAR_MINUS_SIGN
        bsf         FLAG_MOTOR_Z, DIRECTION
        _m_SkipIfNOMatch    CHAR_TEMP, _CHAR_PLUS_SIGN
        bcf         FLAG_MOTOR_Z, DIRECTION
        ;;
        call        ResetCONV_INPUT
        _m_ReadString       4, CONV_INPUT
        call        AsciiConvertDec2Bin
        _m_Mov16    CONV_RESULT, FUNCTION_Z
        BANKSEL     FLAG_MOTOR_Z
        bsf         FLAG_MOTOR_Z,ENABLE

        goto        ParseTestFun
;;; ------------------------------------------------------------------------------------------------
;;;
;;;
ParseFunM:
        call        ResetCONV_INPUT
        _m_ReadString       2, CONV_INPUT
        call        AsciiConvertDec2Bin
        _m_Mov16    CONV_RESULT, FUNCTION_M

;;; _m_BranchOnMatch    FUNCTION_M, 0x00, FunM00
;;; _m_BranchOnMatch    FUNCTION_M, 0x01, FunM01

        goto        ParseTestFun
;;; ------------------------------------------------------------------------------------------------
;;;
;;;
ParseFunF:
        call        ResetCONV_INPUT
        _m_ReadString       4, CONV_INPUT
        call        AsciiConvertDec2Bin
        _m_Mov16    CONV_RESULT, FUNCTION_F

        goto        ParseTestFun
;;; ------------------------------------------------------------------------------------------------
ParseBUFFER_BLOCK_End:
        call        ResetBUFFER_BLOCK
        return

;;; ================================================================================================
;;;                                         ROTINA : ComputeFUNCTION_X
;;;
;;;
;;; ================================================================================================
ComputeFUNCTION_X:
        _m_Mov16    FUNCTION_X, MOTOR_X_COUNTER
        _m_Clr16    FUNCTION_X
        return

ComputeFUNCTION_Y:
        _m_Mov16    FUNCTION_Y, MOTOR_Y_COUNTER
        _m_Clr16    FUNCTION_Y
        return

ComputeFUNCTION_Z:
        _m_Mov16    FUNCTION_Z, MOTOR_Z_COUNTER
        _m_Clr16    FUNCTION_Z
        return

;;; ================================================================================================
;;;                                      ROTINA : ComputeFUNCTION_F
;;;
;;; Calcula a velocidade de avanco de cada eixo (x,y,z iguais)
;;; ------------------------------------------------------------------------------------------------
;;;
;;; Feedrate = mm/min
;;; ex: F0010 = 10mm/min
;;;
;;; PARAMETROS:
;;;                 Ang_MotorPasso  (ex: 7,5 graus/passo)
;;;                 Passo_fuso      (ex: 1mm/volta)
;;;                 deltaX = (Ang_MotorPasso * Passo_fuso)/360
;;;
;;;
;;; ================================================================================================
ComputeFUNCTION_F:
        BANKSEL     FUNCTION_F
        movfw       FUNCTION_F
        btfss       MAIN_FLAG,G01
        movlw       .10
        BANKSEL     STEP_DELAY
        movwf       STEP_DELAY

        return


  END

;;;                                            FIM DO ARQUIVO

