;;; Autor: Pedro Maione
;;; Goiania, Dezembro de 2015


;;;                                           INCLUDE HEADERS
  include         "p16f877a.inc"

;;; MAIN_FLAG bits
DEV_BUSY                        EQU     0
G01                             EQU     1

;;; Define os bits dos FLAGs de BUFFERS
FULL                            EQU     0
EMPTY                           EQU     1
EOL                             EQU     2
BUSY                            EQU     3
PARAMETER                       EQU     6
COMMAND                         EQU     7

;;; Define os bits dos FlagsStepX,Y,Z
ENABLE                          EQU     0
DIRECTION                       EQU     1

;;;                                          ENTRADAS / SAIDAS
  #define PORT_LED                PORTA
  #define LED_ERRO                PORT_LED,1
  #define LED_RUN                 PORT_LED,2
  #define LED_INT                 PORT_LED,3
  #define _MOTORX                 PORTD
  #define _MOTORY                 PORTD
  #define _MOTORZ                 PORTC

;;;                                              CONSTANTES

;;; ================================================================================================
_MOTOR_X_MASK                   SET     0xF0
_MOTOR_Y_MASK                   SET     0x0F
_MOTOR_Z_MASK                   SET     0xF0

_MOTOR_X_NIBBLE                 SET     0 ; Low Nibble
_MOTOR_Y_NIBBLE                 SET     1 ; Low Nibble
_MOTOR_Z_NIBBLE                 SET     0 ; Low Nibble

;;; ================================================================================================
_CHAR_RX_EOL                    SET     0x0D ; ASCII 'CR' - Carriage Return

_CHAR_ER_RX_OVER                SET     0xC0 ; Codigo de erro de Overrun
_CHAR_ER_RX_FRAM                SET     0xC1 ; Codigo de erro de Framing
_CHAR_ER_RX_BUFF_FULL           SET     0xC2 ; Codigo de erro de Buffer Cheio

_CHAR_COMMAND                   SET     0x40 ; ASCII '@' Caractere : incio de um bloco de comando
_CHAR_PARAM                     SET     0x25 ; ASCII '%' Caractere : define um parametro

_CHAR_BLOCK_EOL                 SET     0x3B ; ASCII ';' Fim de uma string de comando

_CHAR_PREP_FUN                  SET     0x47 ; ASCCI 'G' Funcao preparatoria
_CHAR_AUX_FUN                   SET     0x4D ; ASCCI 'M' Funcao auxiliar
_CHAR_FEED_FUN                  SET     0x46 ; ASCCI 'F' Funcao 'Feed Rate'

_CHAR_X_FUN                     SET     0x58 ; ASCII 'X' Deslocamento eixo x
_CHAR_Y_FUN                     SET     0x59 ; ASCII 'Y' Deslocamento eixo y
_CHAR_Z_FUN                     SET     0x5A ; ASCII 'Z' Deslocamento eixo z

_CHAR_PLUS_SIGN                 SET     0x2B ; ASCII '+' Sentido de deslocamento
_CHAR_MINUS_SIGN                SET     0x2D ; ASCII '-' Sentido de deslocamento



_m_SetErrorLED      MACRO
        BANKSEL     PORT_LED
        bsf         LED_ERRO
        ENDM

_m_ClearErrorLED    MACRO
        BANKSEL     PORT_LED
        bcf         LED_ERRO
        ENDM

_m_SetLED_RUN       MACRO
        BANKSEL     PORT_LED
        bsf         LED_RUN
        ENDM

_m_ClearLED_RUN     MACRO
        BANKSEL     PORT_LED
        bcf         LED_RUN
        ENDM

